<?php
/**
 * Admin Options ( Register Field Groups for Sermons Plugin Settings )
 * @package		ChurchAmp_Sermons
 * @subpackage  	Admin
 * @version		5.0.0
 * @since			1.0.0
 * @author		Endeavr Media <support@endeavr.com>
 * @copyright		Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link			http://churchamp.com/plugins/sermons
 * @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * The Admin Options require the Advanced Custom Fields plugin and its Options add-on be activated.
 */

if(function_exists("register_options_page"))
{
    register_options_page('Sermons');
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_sermon-plugin-settings',
		'title' => 'Sermon Plugin Settings',
		'fields' => array (
			array (
				'key' => 'endvr_set_label_singular_sermons',
				'label' => 'Label (Singular / Lowercase)',
				'name' => 'endvr_set_label_singular_sermons',
				'type' => 'text',
				'instructions' => 'This sets the singular label for the Sermons Plugin. This defaults to "sermon," but this field is available in case it is preferable to call it something else like "message."',
				'default_value' => 'sermon',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_label_singular_capital_sermons',
				'label' => 'Label (Singular / Capitalized)',
				'name' => 'endvr_set_label_singular_capital_sermons',
				'type' => 'text',
				'instructions' => 'This is the capitalized iteration of whatever is set in the lowercase singular label. For example, if the lowercase singular label is "sermon," then the capitalized version input in this field would be "Sermon."',
				'default_value' => 'Sermon',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_label_plural_sermons',
				'label' => 'Label (Plural / Lowercase)',
				'name' => 'endvr_set_label_plural_sermons',
				'type' => 'text',
				'instructions' => 'This sets the plural label for the Sermons Plugin. This defaults to "sermons," but this field is available in case it is preferable to call it something else like "messages."',
				'default_value' => 'sermons',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_label_plural_capital_sermons',
				'label' => 'Label (Plural / Capitalized)',
				'name' => 'endvr_set_label_plural_capital_sermons',
				'type' => 'text',
				'instructions' => 'This is the capitalized iteration of whatever is set in the lowercase plural label. For example, if the lowercase plural label is "sermons," then the capitalized version input in this field would be "Sermons."',
				'default_value' => 'Sermons',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_archive_slug_sermons',
				'label' => 'Sermons Archive URL Slug',
				'name' => 'endvr_set_archive_slug_sermons',
				'type' => 'text',
				'instructions' => 'Input the URL Slug for the custom post type as it should appear when viewing the archive page.',
				'default_value' => 'sermons',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_base_slug_sermons',
				'label' => 'Sermon Base URL Slug',
				'name' => 'endvr_set_base_slug_sermons',
				'type' => 'text',
				'instructions' => 'Typically, this will be the same as the Archive URL Slug. But, if for some reason, the custom post type\'s	base slug should be different than the archive slug when the individual entry\'s slug is appended to the custom post type\'s base slug, then this field allows for that.',
				'default_value' => 'sermons',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_label_singular_capital_sermons_tax_series',
				'label' => 'Series Label (Singular / Capitalized)',
				'name' => 'endvr_set_label_singular_capital_sermons_tax_series',
				'type' => 'text',
				'instructions' => 'This sets the singular capitalized label for the Sermon Series taxonomy. This defaults to "Series," but this field is available in case it is preferable to call it something else.',
				'default_value' => 'Series',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_label_plural_capital_sermons_tax_series',
				'label' => 'Series Label (Plural / Capitalized)',
				'name' => 'endvr_set_label_plural_capital_sermons_tax_series',
				'type' => 'text',
				'instructions' => 'This sets the plural capitalized label for the Sermon Series taxonomy. This defaults to "Series," but this field is available in case it is preferable to call it something else.',
				'default_value' => 'Series',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_base_slug_sermons_tax_series',
				'label' => 'Series Base URL Slug',
				'name' => 'endvr_set_base_slug_sermons_tax_series',
				'type' => 'text',
				'instructions' => 'This is the Base URL Slug for the Sermon Series taxonomy.',
				'default_value' => 'series',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_label_singular_capital_sermons_tax_speaker',
				'label' => 'Speaker Label (Singular / Capitalized)',
				'name' => 'endvr_set_label_singular_capital_sermons_tax_speaker',
				'type' => 'text',
				'instructions' => 'This sets the singular capitalized label for the Sermon Speaker taxonomy. This defaults to "Speaker," but this field is available in case it is preferable to call it something else.',
				'default_value' => 'Speaker',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_label_plural_capital_sermons_tax_speaker',
				'label' => 'Speaker Label (Plural / Capitalized)',
				'name' => 'endvr_set_label_plural_capital_sermons_tax_speaker',
				'type' => 'text',
				'instructions' => 'This sets the plural capitalized label for the Sermon Speaker taxonomy. This defaults to "Speakers," but this field is available in case it is preferable to call it something else.',
				'default_value' => 'Speakers',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_base_slug_sermons_tax_speaker',
				'label' => 'Speaker Base URL Slug',
				'name' => 'endvr_set_base_slug_sermons_tax_speaker',
				'type' => 'text',
				'instructions' => 'This is the Base URL Slug for the Sermon Speaker taxonomy.',
				'default_value' => 'speaker',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_label_singular_capital_sermons_tax_scripture',
				'label' => 'Scripture Label (Singular / Capitalized)',
				'name' => 'endvr_set_label_singular_capital_sermons_tax_sripture',
				'type' => 'text',
				'instructions' => 'This sets the singular capitalized label for the Sermon Series taxonomy. This defaults to "Scripture," but this field is available in case it is preferable to call it something else.',
				'default_value' => 'Scripture',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_label_plural_capital_sermons_tax_scripture',
				'label' => 'Scripture Label (Plural / Capitalized)',
				'name' => 'endvr_set_label_plural_capital_sermons_tax_scripture',
				'type' => 'text',
				'instructions' => 'This sets the plural capitalized label for the Sermon Series taxonomy. This defaults to "Scriptures," but this field is available in case it is preferable to call it something else.',
				'default_value' => 'Scriptures',
				'formatting' => 'none',
			),
			array (
				'key' => 'endvr_set_base_slug_sermons_tax_scripture',
				'label' => 'Scripture Base URL Slug',
				'name' => 'endvr_set_base_slug_sermons_tax_scripture',
				'type' => 'text',
				'instructions' => 'This is the Base URL Slug for the Sermon Scripture taxonomy.',
				'default_value' => 'scripture',
				'formatting' => 'none',
			),
		),
		'location' => array (
			'rules' => array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options-sermons',
					'order_no' => 0,
				),
			),
			'allorany' => 'all',
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}