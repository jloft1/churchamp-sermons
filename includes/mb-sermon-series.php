<?php
/**
 * Meta Boxes ( Register Field Groups for the Sermon Series Taxonomy )
 *
 * @package  		ChurchAmp_Sermons
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/sermons
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * The Meta Boxes require the Advanced Custom Fields plugin be activated.
 *
 * The register_field_group function accepts 1 array which holds the relevant data to register a field group
 * You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 * This code must run every time the functions.php file is read
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_taxonomy-image',
		'title' => 'Taxonomy Image',
		'fields' => array (
			0 => array (
				'key' => 'field_514c603d11243',
				'label' => 'Taxonomy Image',
				'name' => '_endvr_taxonomy_image_sermonseries',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
			),
		),
		'location' => array (
			'rules' => array (
				0 => array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'sermonseries',
					'order_no' => 0,
				),
			),
			'allorany' => 'all',
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}