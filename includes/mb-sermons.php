<?php
/**
 * Meta Boxes ( Register Field Groups for the Sermons CPT )
 *
 * @package    ChurchAmp_Sermons
 * @subpackage   Includes
 * @version    5.0.0
 * @since     1.0.0
 * @author    Endeavr Media <support@endeavr.com>
 * @copyright   Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link     http://churchamp.com/plugins/sermons
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * The Meta Boxes require the Advanced Custom Fields plugin and its Repeater add-on be activated.
 *
 * The register_field_group function accepts 1 array which holds the relevant data to register a field group
 * You may edit the array as you see fit. However, this may result in errors if the array is not compatible with ACF
 * This code must run every time the functions.php file is read
 */

if(function_exists("register_field_group"))
{
	register_field_group(array (
			'id' => 'acf-sermons-details',
			'title' => 'Sermon Details',
			'fields' =>
			array (
				0 =>
				array (
					'key' => '_endvr_sermon_audio',
					'label' => 'Sermon Audio File',
					'name' => '_endvr_sermon_audio',
					'type' => 'file',
					'order_no' => 0,
					'instructions' => 'Upload the MP3 audio file for the sermon.',
					'required' => 0,
					'conditional_logic' =>
					array (
						'status' => 0,
						'rules' =>
						array (
							0 =>
							array (
								'field' => 'null',
								'operator' => '==',
								'value' => '',
							),
						),
						'allorany' => 'all',
					),
					'save_format' => 'url',
				),
				1 =>
				array (
					'key' => '_endvr_sermon_video',
					'label' => 'Sermon Video (Vimeo Embed)',
					'name' => '_endvr_sermon_video',
					'type' => 'text',
					'order_no' => 1,
					'instructions' => 'Paste into this text field the Vimeo.com URL for the sermon video. (Do not use the embed code. Just the URL.)',
					'required' => 0,
					'conditional_logic' =>
					array (
						'status' => 0,
						'rules' =>
						array (
							0 =>
							array (
								'field' => 'null',
								'operator' => '==',
								'value' => '',
							),
						),
						'allorany' => 'all',
					),
					'default_value' => '',
					'formatting' => 'none',
				),
				2 =>
				array (
					'key' => '_endvr_sermon_doc',
					'label' => 'Sermon Outline Document',
					'name' => '_endvr_sermon_doc',
					'type' => 'file',
					'order_no' => 2,
					'instructions' => 'Upload the sermon outline document as a PDF.',
					'required' => 0,
					'conditional_logic' =>
					array (
						'status' => 0,
						'rules' =>
						array (
							0 =>
							array (
								'field' => 'null',
								'operator' => '==',
								'value' => '',
							),
						),
						'allorany' => 'all',
					),
					'save_format' => 'url',
				),
				3 =>
				array (
					'key' => '_endvr_sermon_ref',
					'label' => 'Sermon Scripture Reference',
					'name' => '_endvr_sermon_ref',
					'type' => 'text',
					'order_no' => 3,
					'instructions' => 'Input into this field the Scripture reference for this sermon. Input format should follow this pattern: John 3:16-21,24',
					'required' => 0,
					'conditional_logic' =>
					array (
						'status' => 0,
						'rules' =>
						array (
							0 =>
							array (
								'field' => 'null',
								'operator' => '==',
								'value' => '',
							),
						),
						'allorany' => 'all',
					),
					'default_value' => '',
					'formatting' => 'none',
				),
			),
			'location' =>
			array (
				'rules' =>
				array (
					0 =>
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sermons',
						'order_no' => 1,
					),
				),
				'allorany' => 'all',
			),
			'options' =>
			array (
				'position' => 'advanced',
				'layout' => 'default',
				'hide_on_screen' =>
				array (
				),
			),
			'menu_order' => 1,
		));
}