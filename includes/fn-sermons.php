<?php
/**
 * Functions File for Sermons CPT ( various functions, filters, and actions used by the plugin )
 *
 * @package  		ChurchAmp_Sermons
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/sermons
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * @example		https://github.com/justintadlock/custom-content-portfolio/blob/master/includes/functions.php
 */

/**
 * Filter to ensure the proper labels display when user updates custom post type entries.
 * translators: %s: date and time of the revision
 * translators: Publish box date format, @example: http://php.net/date
 *
 * @since  0.1.0
 * @access public
 * @param  string $title
 * @return string
 */
add_filter( 'post_updated_messages', 'endvr_updated_messages_sermons' );
function endvr_updated_messages_sermons( $messages ) {
global $post, $post_ID;
	$messages['sermons'] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => sprintf( __('Sermon Updated. <a href="%s">View Sermon</a>'), esc_url( get_permalink($post_ID) ) ),
		2  => __('Sermon Updated.'),
		3  => __('Sermon Deleted.'),
		4  => __('Sermon Updated.'),
		5  => isset($_GET['revision']) ? sprintf( __('Sermon Restored to Revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => sprintf( __('Sermon Published. <a href="%s">View Sermon</a>'), esc_url( get_permalink($post_ID) ) ),
		7  => __('Sermon Saved.'),
		8  => sprintf( __('Sermon Submitted. <a target="_blank" href="%s">Preview Sermon</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9  => sprintf( __('Sermon Scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Sermon</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10  => sprintf( __('Sermon Draft Updated. <a target="_blank" href="%s">Preview Sermon</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}

/**
 * Filter on 'post_type_archive_title' to allow for the use of the 'archive_title' label.
 * Not supported by WordPress natively, but that's okay since we can roll our own labels.
 *
 * @since  0.1.0
 * @access public
 * @param  string $title
 * @return string
 */

add_filter( 'post_type_archive_title', 'endvr_post_type_archive_title_sermons' );
function endvr_post_type_archive_title_sermons( $title ) {
	if ( is_post_type_archive( 'sermons' ) ) {
		$post_type = get_post_type_object( 'sermons' );
		$title = isset( $post_type->labels->archive_title ) ? $post_type->labels->archive_title : $title;
	}
	return $title;
}

/**
 * Filter on 'post_type_link' to allow the use of '%sermonseries%' (the 'sermonseries' taxonomy)
 * in the sermon URLs.
 *
 * @since  0.1.0
 * @access public
 * @param  string $post_link
 * @param  object $post
 * @return string
 */

add_filter( 'post_type_link', 'endvr_post_type_link_sermons', 10, 2 );
function endvr_post_type_link_sermons( $post_link, $post ) {

	if ( 'sermons' !== $post->post_type )
		return $post_link;

	/* Allow %sermonseries% in the custom post type permalink. */
	if ( false !== strpos( $post_link, '%sermonseries%' ) ) {

		/* Get the terms. */
		$terms = get_the_terms( $post, 'sermonseries' ); // @todo apply filters to tax name.

		/* Check that terms were returned. */
		if ( $terms ) {

			usort( $terms, '_usort_terms_by_ID' );

			$post_link = str_replace( '%sermonseries%', $terms[0]->slug, $post_link );

		} else {
			$post_link = str_replace( '%sermonseries%', 'item', $post_link );
		}
	}

	return $post_link;
}

/**
 * Filters the 'breadcrumb_trail_items' hook from the Breadcrumb Trail plugin and the script version
 * included in the Hybrid Core framework.  At best, this is a neat hack to add the sermons to the
 * single view of sermons based off the '%sermonseries%' rewrite tag.  At worst, it's potentially
 * a huge management nightmare in the long term.  A better solution is definitely needed baked right
 * into Breadcrumb Trail itself that takes advantage of its built-in features for figuring out this type
 * of thing.
 *
 * @since  0.1.0
 * @access public
 * @param  array  $items
 * @return array
 */

add_filter( 'breadcrumb_trail_items', 'endvr_breadcrumb_trail_items_sermons' );
function endvr_breadcrumb_trail_items_sermons( $items ) {

	if ( is_singular( 'sermons' ) ) {

		if ( false !== strpos( $base_series, '%sermonseries%' ) ) {
			$post_id = get_queried_object_id();

			$terms = get_the_terms( $post_id, 'sermonseries' );

			if ( !empty( $terms ) ) {

				usort( $terms, '_usort_terms_by_ID' );
				$term = get_term( $terms[0], 'sermonseries' );
				$term_id = $term->term_id;

				$parents = array();

				while ( $term_id ) {

					/* Get the parent term. */
					$term = get_term( $term_id, 'sermonseries' );

					/* Add the formatted term link to the array of parent terms. */
					$parents[] = '<a href="' . get_term_link( $term, 'sermonseries' ) . '" title="' . esc_attr( $term->name ) . '">' . $term->name . '</a>';

					/* Set the parent term's parent as the parent ID. */
					$term_id = $term->parent;
				}

				$items   = array_splice( $items, 0, -1 );
				$items   = array_merge( $items, array_reverse( $parents ) );
				$items[] = single_post_title( '', false );
			}
		}
	}

	return $items;
}

/**
 * Filter to assign custom text instructions on publish form to replace "Enter title here".
 *
 * @since  0.1.0
 * @access public
 * @param  string $title
 * @return string
 */

add_filter( 'enter_title_here', 'endvr_replace_title_label_sermons' );
function endvr_replace_title_label_sermons( $title ) {
	$screen = get_current_screen();
	if  ( 'sermons' == $screen->post_type ) {
		$title = 'Enter Sermon Title Here...';
		return $title;
	}
}

/**
 * Assign a custom label to the Sub Title.
 *
 * @since  0.1.0
 * @access public
 * @example: http://codex.wordpress.org/Function_Reference/remove_filter
 */

add_action( 'admin_head', 'endvr_replace_subtitle_label_sermons' );
function endvr_subtitle_label_sermons() {
	echo 'Input a Subtitle for this Sermon (Optional)';
}
function endvr_replace_subtitle_label_sermons() {
	$screen = get_current_screen();
	if  ( 'sermons' == $screen->post_type ) {
		remove_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_default' );
		add_filter( 'endvr_subtitle_label', 'endvr_subtitle_label_sermons' );
	}
}

/**
 * Assign a custom label to the Post Editor + place it in a sortable Meta Box.
 *
 * @since  0.1.0
 * @access public
 * @example: http://wordpress.org/support/topic/move-custom-meta-box-above-editor?replies=17
 * @example: http://software.troydesign.it/php/wordpress/move-wp-visual-editor.html
 */

add_action( 'add_meta_boxes', 'endvr_add_meta_box_editor_sermons', 0 );
function endvr_add_meta_box_editor_sermons() {
	$screen = get_current_screen();
	if  ( 'sermons' == $screen->post_type ) {
		global $_wp_post_type_features;
		foreach ($_wp_post_type_features as $type => &$features) {
			if (isset($features['editor']) && $features['editor']) {
				unset($features['editor']);
				add_meta_box(
					'endvr_editor_description_sermons',
					__('Sermon Summary/Description'),
					'endvr_meta_box_editor_sermons',
					$type, 'normal', 'core'
				);
			}
		}
	}
	add_action( 'admin_head', 'endvr_admin_head_sermons' ); //white background
}
function endvr_admin_head_sermons() {
?>
	<style type="text/css">
		.wp-editor-container{background-color:#fff;}
	</style>
<?php
}
function endvr_meta_box_editor_sermons( $post ) {
	echo '<div class="wp-editor-wrap">';
	wp_editor($post->post_content, 'content', array('dfw' => true, 'tabindex' => 1) );
	echo '</div>';
}

/**
 * Redefine the way the content type's admin panel index listing is displayed.
 *
 * @since  0.1.0
 * @access public
 * @example: http://codex.wordpress.org/Plugin_API/Action_Reference/manage_posts_custom_column
 * @example: http://thinkvitamin.com/code/create-your-first-wordpress-custom-post-type/
 */

add_filter( 'manage_edit-sermons_columns', 'endvr_set_custom_edit_columns_sermons' );
add_action( 'manage_sermons_posts_custom_column',  'endvr_custom_column_sermons', 10, 2 );

function endvr_set_custom_edit_columns_sermons($columns) {
	// unset($columns['date']); // we want to show the date for sermons
	return $columns
		+ array(
		'_endvr_sermon_audio'  	=> __('Audio'),
		'_endvr_sermon_video'  	=> __('Video'),
		'_endvr_sermon_doc'  	=> __('Outline'),
		'_endvr_sermon_ref'  	=> __('Reference'),
	);
}

function endvr_custom_column_sermons($column, $post_id) {
	switch ( $column ) {
	case '_endvr_sermon_audio':
		echo get_post_meta( $post_id , '_endvr_sermon_audio' , true );
		break;
	case '_endvr_sermon_video':
		echo get_post_meta( $post_id , '_endvr_sermon_video' , true );
		break;
	case '_endvr_sermon_doc':
		echo get_post_meta( $post_id , '_endvr_sermon_doc' , true );
		break;
	case '_endvr_sermon_ref':
		echo get_post_meta( $post_id , '_endvr_sermon_ref' , true );
		break;
	}
}