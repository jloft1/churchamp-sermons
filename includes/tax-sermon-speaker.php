<?php
/**
 * Taxonomy ( Register Sermon Speaker )
 *
 * @package  		ChurchAmp_Sermons
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/sermons
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* register and define the taxonomy on the 'init' hook */
/* @example: http://codex.wordpress.org/Function_Reference/register_taxonomy */
add_action( 'init', 'endvr_register_tax_sermonspeaker' );
function endvr_register_tax_sermonspeaker() {

	$labels = array(
		'name'                       	=> __( 'Speakers',                           		'churchamp-sermons' ),
		'singular_name'              	=> __( 'Speaker',                            		'churchamp-sermons' ),
		'menu_name'                  	=> __( 'Speakers',                           		'churchamp-sermons' ),
		'name_admin_bar'             	=> __( 'Speakers',                            		'churchamp-sermons' ),
		'search_items'               	=> __( 'Search '.'Speakers'.'',                   	'churchamp-sermons' ),
		'popular_items'              	=> __( 'Popular '.'Speakers'.'',                  	'churchamp-sermons' ),
		'all_items'                  	=> __( 'All '.'Speakers'.'',                      	'churchamp-sermons' ),
		'edit_item'                  	=> __( 'Edit '.'Speaker'.'',                      	'churchamp-sermons' ),
		'view_item'                  	=> __( 'View '.'Speaker'.'',                      	'churchamp-sermons' ),
		'update_item'                	=> __( 'Update '.'Speaker'.'',                    	'churchamp-sermons' ),
		'add_new_item'               	=> __( 'Add New '.'Speaker'.'',                   	'churchamp-sermons' ),
		'new_item_name'             	=> __( 'New '.'Speaker'.' Name',                		'churchamp-sermons' ),
		'separate_items_with_commas' 	=> __( 'Separate '.'Speakers'.' with Commas',      	'churchamp-sermons' ),
		'add_or_remove_items'        	=> __( 'Add or Remove '.'Speakers'.'',             	'churchamp-sermons' ),
		'choose_from_most_used'      	=> __( 'Choose from the Most Used '.'Speakers'.'',	'churchamp-sermons' ),
	);
	/* only 2 caps are needed: 'manage_sermons' and 'edit_sermons'. */
	$capabilities = array(
		'manage_terms' 			=> 'manage_sermons',
		'edit_terms'   			=> 'manage_sermons',
		'delete_terms' 			=> 'manage_sermons',
		'assign_terms' 			=> 'edit_sermons',
	);
	$rewrite = array(
		'slug'         			=> 'sermons/speaker',
		'with_front'   			=> false,
		'hierarchical' 			=> false,
		'ep_mask'      			=> EP_NONE,
	);
	$args = array(
		'public'            		=> true,
		'show_ui'           		=> true,
		'show_in_nav_menus' 		=> true,
		'show_tagcloud'     		=> false,
		'show_admin_column' 		=> true,
		'hierarchical'      		=> true,
		'query_var'         		=> 'sermonspeaker',
		'capabilities' 			=> $capabilities,
		'rewrite' 				=> $rewrite,
		'labels' 					=> $labels,
	);

	/* register the 'sermonspeaker' taxonomy. */
	register_taxonomy( 'sermonspeaker', array( 'sermons' ), $args );
}